import numpy as np
from matplotlib import pyplot as plt

def initpopulation(N, lims=(0,100)):
    '''
    Initializes a population of N individuals within given range
    N       : number of individuals in a population
    range   : min, max values for the individuals
    '''
    pop = np.random.rand(N,2)
    pop = lims[0] + pop * lims[1]
    return pop

def fitness(func, pop):
    '''
    Returns fitness for each individual as an numpy array
    func    : function that specifies the fitness with population arguements
    pop     : population of individuals
    '''
    perf = np.zeros(len(pop))
    for i , ind in enumerate(pop):
        perf[i] = func(ind)
    return perf

def children(pop, perf):
    '''
    Creates new generation based on the performance of individual from previous generation
    pop     : previous population
    perf    : performance of the individuals in the previous population
    '''
    newpop = np.zeros(np.shape(pop))
    pop = pop[perf.argsort()]
    for i in xrange(len(pop)):
        if i<len(pop)/10:
            # best performing individuals directly go to next generation
            # here top 10% survives
            newpop[i] = pop[i]
        else:
            nchildren = int(len(pop))
            child1 = pop[0,:]
            child2 = pop[1,:]
            # Check if the children already exist in the new population
            while np.any(newpop == child1) or np.any(newpop == child2):
                p1 = np.random.randint(nchildren/2)
                p2 = np.random.randint(nchildren)
                while p1==p2:
                    p2 = np.random.randint(nchildren)
                p1c = chrom(pop[p1])
                p2c = chrom(pop[p2])
                c1, c2 = crossover(p1c, p2c)
                child1 = val(mutate(c1))
                child2 = val(mutate(c2))
            if np.any(newpop==child1):
                newpop[i] = child2
            else:
                newpop[i] = child1
    return newpop

def mutate(chrom, mp=0.1):
    '''
    Mutate the new individual with a mutation probability
    chrom   : chromosome representation of an individual
    mp      : mutation probability
    '''
    if np.random.rand()<mp:
        chrom[np.random.randint(0, len(chrom))] = np.random.randint(0, 10) 
    return chrom
           
def val(chrom):
    '''
    Represent a set of chromosomes as an individual
    chrom   : chromosome representation of individuals
    '''
    ind = np.zeros((2))
    for i in xrange(15):
        ind[1] += chrom[-i-1]*(10**(i+1))
        ind[0] += chrom[-16-i]*(10**(i+1))
    return ind*1e-13

def chrom(ind):
    '''
    Represent an individual as a set of chromosomes
    ind     : individual values
    '''
    chromarray = np.zeros((30))
    for i in xrange(15):
        chromarray[i] = int(np.mod(ind[0]*(10**(i-2)), 10))
        chromarray[15+i] = int(np.mod(ind[1]*(10**(i-2)),10))
    return chromarray

def crossover(chrom1, chrom2):
    '''
    Creates 2 children from 2 given parents
    chrom1  : parent 1
    chrom2  : parent 2
    '''
    copoint = np.random.random_integers(1, len(chrom1)-1)
    child1 = np.append(chrom1[:copoint], chrom2[copoint:])
    child2 = np.append(chrom2[:copoint], chrom1[copoint:])
    return child1, child2

def minimizedist(ind):
    '''
    Function calculates the distance between target and an individual
    takes an indivual and returns function value
    ind     : individual
    '''
    target = [20, 70]
    return np.sqrt((ind[0]-target[0])**2+(ind[1]-target[1])**2)

def genetic(func=minimizedist, N=20, tolerance=2):
    '''
    Example code to use genetic algorithm to minimize function minimizedist
    '''

    pop = initpopulation(N)
    perf = fitness(func, pop)

    tol = np.min(perf)
    while tol > tolerance:
        pop = children(pop, perf)
        perf = fitness(func, pop)
        tol = np.min(perf)
        print 'Distance: ', tol, ' x, y: ', pop[np.where(perf==np.min(perf))[0]]

    return pop[np.where(perf==np.min(perf))[0]]
