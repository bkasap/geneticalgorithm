# README #

Here is an example usage of genetic algorithm to find the minimum of a given function. 

### How do I get set up? ###

The code is given in ga.py with an example running function genetic().
The example usage is given in the notebook (.ipynb) file.
Plots are also provided.

Notice, that the genetic algorithms use an heuristic approach. The solution might not be optimal and applicable to all questions. Depending on the random generation of the initial population, simulations might fail or take longer.

#### Best use ####

After cloning the repository, reach for the source folder and run:

ipython notebook

#### Dependencies ####
numpy, pandas, pylab